# Description #

Utility to check usage of HUR (Hitachi Universal Replicator) journals.  Useful to monitor journal volume usage and trending.

### Overview of check_jnl.sh ###

Intended to be ran on Unix or Linux computer and ran non-interactively via cron(8) using crontab(5) command.  Script will send an Alert if the user defined threshold $AlertLevel is exceeded.  Alerts are sent via STDOUT from script, logger command to syslog, and email.  Each time script is ran the journal usage is updated in a CSV file $LogFile.  The logfile is useful for trending and long term analysis.

### Getting Started / Setup / Configuration ###

The section 'User Definable Settings' should be updated within the script check_jnl.sh to achieve the appropriate behaviour for the utility.  The following variables should be evaluated prior to usage.

 # Set HORCM instance
 HorcmInst="0"

 # Alert % - If Jnl usage is > than defined value
 AlertLevel="50"

 # Logfile to keep track of Jnl Usage, CSV format
 LogFile="/var/log/JnlLog.out"

 # Email address to send Alerts to.
 # If you don't want email Alerts then
 # set variable to nothing i.e. Email=""
 Email=""

### Pre-Requisites for Usage ###
* Requires HDS Enterprise storage system i.e. USP-V, VSP, G1000, VSP G
* Requires the use of HDS CCI Software.
* CCI (Command Control Interface) must be installed with a working horcm file (i.e. /etc/horcm0.conf) to provide communication to HDS array.

### Workflow of check_jnl.sh ###
* Obtains HUR Journal usage via raidcom command.
* Logs Journal usage detail to log file for trending.
* Sends out Alert if Journal usage threshold is exceeded.
* Alert consists of syslog message, and email notification.

### Customization Ideas ###
If appropriate update the function logerr() to send
  Alert message via external programs.
      i.e. open support ticket, etc.

### Related Script Ideas ###
* data visualization of Journal Usage trends
* monitor RPO compliance versus replication states
* monitor communications paths
* replication health of volumes, i.e. verify vols are in expected replication state
* bootstrap script that builds replication configurations



### License, Warranty ###
As is.  No warranty.

* Covered under MIT License.  https://opensource.org/licenses/MIT

### Collaboration ###
Always looking to partner with other people on developing this and other related utilities.  Specifically with the following areas:
* Writing tests
* Code review
* Testing
* Feedback and Review

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact