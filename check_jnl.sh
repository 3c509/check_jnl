#!/bin/sh
##################################################
# check_jnl.sh 
#
# version:          0.2 - 1 July 2016
# Original Author:  Alex Theodore, 
#                   atheodore@edge-solutions.com
#
# Description:
#
# Utility to check usage of HUR journals.
#
# Intended to be ran on Unix or Linux computer and
# ran non-interactively via cron(8) using crontab(5)
# command. 
#
# See seciont User Modifiable Settings for initial configuration.
#
#
# Requirements:
# Requires the use of HDS CCI Software.
# CCI must be configured for communication using /etc/horcm?.conf
# 
# Workflow:
# Obtains HUR Journal usage via raidcom command.
# Logs Journal usage detail to log file for trending.
# Sends out Alert if Journal usage threshold is exceeded.
# Alert consists of syslog message, and email notification.
#
# User Modifiable Settings:
#
# The section User Definable Settings should be updated
# to achieve the appropriate behaviour for the utility.
#
# Customization Ideas:
#
# If appropriate update the function logerr() to send
# Alert message via external programs.  
#       i.e. open support ticket, etc.
#
#
# Additional Script Ideas:
#
#       - data visualization of Journal Usage trends
#       - monitor RPO compliance versus replication states
#       - monitor communications paths
#       - replication health of volumes, 
#               i.e. verify vols are in expected replication state
#       - bootstrap script that builds replication configurations
#
#
# As is. No warranty. 
#
#       Covered under MIT License.
#       https://opensource.org/licenses/MIT
#
##################################################



#######[User Definable Settings]#######


# Set HORCM instance
HorcmInst="0"

# Alert % - If Jnl usage is > than defined value
AlertLevel="50"

# Logfile to keep track of Jnl Usage, CSV format
LogFile="/var/log/JnlLog.out"

# Email address to send Alerts to.
# If you don't want email Alerts then 
# set variable to nothing i.e. Email=""
Email=""




#######[Script Funcitons]#######

# Starts HORCM for communication to array
starthorcm() {
  HorcmFile="/etc/horcm$HorcmInst.conf"
  if [ ! -f $HorcmFile ]; then
    echo "ERROR: Cannot find HORCM config file: $HorcmFile"
    exit 1
  fi

  if [ ! -f /HORCM/usr/bin/raidcom ]; then
    echo "ERROR: Cannot find command raidcom in /HORCM/usr/bin"
    exit 1
  fi

  /HORCM/usr/bin/horcmstart.sh $HorcmInst > /dev/null 2>&1 
  if [ $? -ne 0 ]; then
    echo "ERROR: HORCM instance $HorcmInst unable to start"
    exit 1
  fi
}

# Checks Journal info via raidcom command and sends alert
jnlstatus() {
  JnlOut=`/HORCM/usr/bin/raidcom get journal | grep -v ^JID | \
  awk '{printf "%s,%d,%d\n", strftime("%Y-%m-%d-%H:%M:%S"),$1,$6}'`

  # Log info to CSV
  /HORCM/usr/bin/raidcom get journal | grep -v ^JID | \
  awk '{printf "%s,%d,%d\n", strftime("%Y-%m-%d-%H:%M:%S"),$1,$6}' \
  >> $LogFile

  # Parse CSV data or raidcom and evaluate Jnl Usage
  for i in `echo $JnlOut`
  do
    JnlID=`echo $i | awk -F, '{print $2}'`
    JnlPct=`echo $i | awk -F, '{print $3}'`

    # Check if journal usage exceeds threshold
    if [ $JnlPct -gt $AlertLevel ]; then
      logerr $JnlID $JnlPct
    fi
  done
}

# Sends Alert when Journal threshold has been exceeded
# function expects two arguments: logerr JournalID JournalPercentUsed
logerr() {
  JnlID=$1
  JnlPct=$2

  msg="ALERT: Journal ID[$JnlID] has exceeded threshold [$AlertLevel] and is at $JnlPct% Used Capacity."
  echo $msg
  logger -t CheckJnl -p local0.notice "$msg"

  if [ ! -z $Email ]; then
    EmailTest=`expr length $Email`
    if [ $EmailTest -gt 0 ]; then
      echo "sending email..."
      echo $msg | mail -s \
      "\"ALERT: Journal[$JnlID] Capacity Threshold Exceeded\"" $Email
    fi 
  fi
}

#######[Main Section]#######
#
#
starthorcm
jnlstatus
exit 0
